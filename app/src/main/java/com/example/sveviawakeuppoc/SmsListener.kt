package com.example.sveviawakeuppoc

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Context.AUDIO_SERVICE
import android.content.Intent
import android.content.SharedPreferences
import android.media.AudioManager
import android.media.RingtoneManager
import android.provider.Telephony.Sms.Intents.getMessagesFromIntent
import android.util.Log
import android.widget.Toast


class SmsListener : BroadcastReceiver() {

    private val preferences: SharedPreferences? = null

    override fun onReceive(context: Context, intent: Intent) {

        Log.d("smsListener: ", "intent: $intent")

        if (intent.action == "android.provider.Telephony.SMS_RECEIVED") {

            try {
                val messages = getMessagesFromIntent(intent)
                val msg = messages.first()


                if (msg.messageBody.contains("TriggerAlarm")) {
                    val text = "OMG! You got a text!"
                    val duration = Toast.LENGTH_SHORT

                    val toast = Toast.makeText(context, text, duration)
                    toast.show()

                    var alert = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM)
                    if (alert == null){
                        // alert is null, using backup
                        alert = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
                        if (alert == null){
                            // alert backup is null, using 2nd backup
                            alert = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE)
                        }
                    }

                    val ringtone = RingtoneManager.getRingtone(context, alert)
                    ringtone.setStreamType(AudioManager.STREAM_ALARM)

                    val audioManager = context.getSystemService(AUDIO_SERVICE) as AudioManager?
                    var volume = audioManager!!.getStreamVolume(AudioManager.STREAM_ALARM)
                    if (volume == 0) {
                        volume = audioManager.getStreamMaxVolume(AudioManager.STREAM_ALARM)
                    }
                    audioManager.setStreamVolume(
                        AudioManager.STREAM_ALARM,
                        volume,
                        AudioManager.FLAG_REMOVE_SOUND_AND_VIBRATE
                    )

                    ringtone?.play()
                }

                Log.d("smsListener: ", "msgBody${msg.messageBody}")


            } catch (e: Exception) {
                Log.d("smsListener: ", "Exception caught: ${e.message}")
            }

        }
    }
}
