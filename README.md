## Description
This is a POC app for Svevia to prove the concept of an SMS-triggered wake-up alarm for infrastructure workers on call.

Using a BroadcastReceiver listening to intents of type `android.provider.Telephony.SMS_RECEIVED`,
we use a `RingtoneManager` to get the users own alarm ringtone, and then an `AudioManager` to play an audiofile.
We set the audioManager's volume with `setStreamVolume`  and set it's `streamType` to `STREAM_ALARM`to make sure the sound plays through even if phone is in silent mode.
The app requests the `RECEIVE_SMS` permission on startup.

## Function:
After installed, this app plays an alarm after receiving a text message containing the phrase "TriggerAlarm" anywhere in the text.
This works even if phone is in silent mode
This works even if app is in background
This works even if app is closed

## Android API support:
AudioManager and STREAM_ALARM: Added in API level 1
BroadcastReceiver intent type "SMS_RECEIVED":  Added in API level 19

This means the app should work for all phones later than API level 19 (Android 4.4 - KitKat) which means [98.7% of Android phones in Sweden](https://gs.statcounter.com/android-version-market-share/mobile-tablet/sweden)